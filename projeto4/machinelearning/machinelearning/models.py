import nn

class PerceptronModel(object):
    def __init__(self, dimensions):
        """
        Initialize a new Perceptron instance.

        A perceptron classifies data points as either belonging to a particular
        class (+1) or not (-1). `dimensions` is the dimensionality of the data.
        For example, dimensions=2 would mean that the perceptron must classify
        2D points.
        """
        self.w = nn.Parameter(1, dimensions)

    def get_weights(self):
        """
        Return a Parameter instance with the current weights of the perceptron.
        """
        return self.w

    def run(self, x):
        """
        Calculates the score assigned by the perceptron to a data point x.

        Inputs:
            x: a node with shape (1 x dimensions)
        Returns: a node containing a single number (the score)
        """
        "*** YOUR CODE HERE ***"
        return nn.DotProduct(self.w, x)

    def get_prediction(self, x):
        """
        Calculates the predicted class for a single data point `x`.

        Returns: 1 or -1
        """
        "*** YOUR CODE HERE ***"
        score = nn.as_scalar(self.run(x))
        return 1 if score >= 0 else -1

    def train(self, dataset):
        """
        Train the perceptron until convergence.
        """
        "*** YOUR CODE HERE ***"
        converged = False
        while not converged:
            converged = True
            for x, y in dataset.iterate_once(1):
                prediction = self.get_prediction(x)
                target = nn.as_scalar(y)
                if prediction != target:
                    self.w.update(x, target)
                    if converged:
                        converged = False

class RegressionModel(object):
    """
    A neural network model for approximating a function that maps from real
    numbers to real numbers. The network should be sufficiently large to be able
    to approximate sin(x) on the interval [-2pi, 2pi] to reasonable precision.
    """
    def __init__(self):
        # Initialize your model parameters here
        "*** YOUR CODE HERE ***"
        self.t_a = .02
        self.p_e_c1 = nn.Parameter(1, 256)
        self.v_c1 = nn.Parameter(1, 256)
        self.p_c1_c2 = nn.Parameter(256, 128)
        self.v_c2 = nn.Parameter(1, 128)
        self.p_c_s = nn.Parameter(128, 1)
        self.v_s = nn.Parameter(1, 1)
        self.params = [self.p_e_c1,self.v_c1,self.p_c1_c2, self.v_c2, self.p_c_s, self.v_s]

    def run(self, x):
        """
        Runs the model for a batch of examples.

        Inputs:
            x: a node with shape (batch_size x 1)
        Returns:
            A node with shape (batch_size x 1) containing predicted y-values
        """
        "*** YOUR CODE HERE ***"
        return nn.AddBias(nn.Linear(nn.ReLU(nn.AddBias(nn.Linear(nn.ReLU(nn.AddBias(nn.Linear(x, self.p_e_c1),self.v_c1)),self.p_c1_c2), self.v_c2)), self.p_c_s), self.v_s)
    
    def get_loss(self, x, y):
        """
        Computes the loss for a batch of examples.

        Inputs:
            x: a node with shape (batch_size x 1)
            y: a node with shape (batch_size x 1), containing the true y-values
                to be used for training
        Returns: a loss node
        """
        "*** YOUR CODE HERE ***"
        return nn.SquareLoss(self.run(x), y)

    def train(self, dataset):
        """
        Trains the model.
        """
        "*** YOUR CODE HERE ***"

        tam_do_lot = 50
        pda_alvo = 0.015
        pda_atual = float('inf')
        
        while pda_atual >= pda_alvo:
            for x, y in dataset.iterate_once(tam_do_lot):
                perda = self.get_loss(x, y)
                print(nn.as_scalar(perda))
                grads = nn.gradients(perda, self.params)
                pda_atual = nn.as_scalar(perda)
                for param, grad in zip(self.params, grads):
                    param.update(grad, -self.t_a)
        

class DigitClassificationModel(object):
    """
    A model for handwritten digit classification using the MNIST dataset.

    Each handwritten digit is a 28x28 pixel grayscale image, which is flattened
    into a 784-dimensional vector for the purposes of this model. Each entry in
    the vector is a floating point number between 0 and 1.

    The goal is to sort each digit into one of 10 classes (number 0 through 9).

    (See RegressionModel for more information about the APIs of different
    methods here. We recommend that you implement the RegressionModel before
    working on this part of the project.)
    """
    def __init__(self):
        # Initialize your model parameters here
        "*** YOUR CODE HERE ***"
        self.t_a = .1
        self.p_e_c1 = nn.Parameter(784, 256)
        self.v_c1 = nn.Parameter(1, 256)
        self.p_c1_c2 = nn.Parameter(256, 128)
        self.v_c2 = nn.Parameter(1, 128)
        self.p_c_s = nn.Parameter(128, 64)
        self.v_s = nn.Parameter(1, 64)
        self.w4 = nn.Parameter(64, 10)
        self.b4 = nn.Parameter(1, 10)
        self.params = [self.p_e_c1,self.v_c1,self.p_c1_c2, self.v_c2,\
                       self.p_c_s, self.v_s, self.w4, self.b4]

    def run(self, x):
        """
        Runs the model for a batch of examples.

        Your model should predict a node with shape (batch_size x 10),
        containing scores. Higher scores correspond to greater probability of
        the image belonging to a particular class.

        Inputs:
            x: a node with shape (batch_size x 784)
        Output:
            A node with shape (batch_size x 10) containing predicted scores
                (also called logits)
        """
        "*** YOUR CODE HERE ***"
        current_layer = x
    
        weights = [self.p_e_c1,self.p_c1_c2, self.p_c_s, self.w4]
        biases = [self.v_c1, self.v_c2, self.v_s, self.b4]

        for i in range(4):
            current_layer = nn.AddBias(nn.Linear(current_layer, weights[i]), biases[i])
            if i < 3:
                current_layer = nn.ReLU(current_layer)
        return current_layer

    def get_loss(self, x, y):
        """
        Computes the loss for a batch of examples.

        The correct labels `y` are represented as a node with shape
        (batch_size x 10). Each row is a one-hot vector encoding the correct
        digit class (0-9).

        Inputs:
            x: a node with shape (batch_size x 784)
            y: a node with shape (batch_size x 10)
        Returns: a loss node
        """
        "*** YOUR CODE HERE ***"
        y_h = self.run(x)
        loss = nn.SoftmaxLoss(y_h, y)
        return loss

    def train(self, dataset):
        """
        Trains the model.
        """
        "*** YOUR CODE HERE ***"
        batch_size = 100
        best_loss = float('inf')
        validation_accuracy = 0

        while validation_accuracy < 0.97:
            for inputs, labels in dataset.iterate_once(batch_size):
                current_loss = self.get_loss(inputs, labels)
                gradients = nn.gradients(current_loss, self.params)
                current_loss = nn.as_scalar(current_loss)
                for param, gradient in zip(self.params, gradients):
                    param.update(gradient, -self.t_a)
            validation_accuracy = dataset.get_validation_accuracy()

class LanguageIDModel(object):
    """
    A model for language identification at a single-word granularity.

    (See RegressionModel for more information about the APIs of different
    methods here. We recommend that you implement the RegressionModel before
    working on this part of the project.)
    """
    def __init__(self):
        # Our dataset contains words from five different languages, and the
        # combined alphabets of the five languages contain a total of 47 unique
        # characters.
        # You can refer to self.num_chars or len(self.languages) in your code
        self.num_chars = 47
        self.languages = ["English", "Spanish", "Finnish", "Dutch", "Polish"]

        # Initialize your model parameters here
        "*** YOUR CODE HERE ***"
        self.t_a = .1
        self.in_w = nn.Parameter(self.num_chars, 128)
        self.ini_b = nn.Parameter(1, 128)
        self.xw = nn.Parameter(self.num_chars, 128)
        self.hw = nn.Parameter(128, 128)
        self.b = nn.Parameter(1, 128)
        self.out_w = nn.Parameter(128, len(self.languages))
        self.outp_b = nn.Parameter(1, len(self.languages))
        self.params = [self.in_w, self.ini_b, self.xw, self.hw, self.b, self.out_w, self.outp_b]

    def run(self, xs):
        """
        Runs the model for a batch of examples.

        Although words have different lengths, our data processing guarantees
        that within a single batch, all words will be of the same length (L).

        Here `xs` will be a list of length L. Each element of `xs` will be a
        node with shape (batch_size x self.num_chars), where every row in the
        array is a one-hot vector encoding of a character. For example, if we
        have a batch of 8 three-letter words where the last word is "cat", then
        xs[1] will be a node that contains a 1 at position (7, 0). Here the
        index 7 reflects the fact that "cat" is the last word in the batch, and
        the index 0 reflects the fact that the letter "a" is the inital (0th)
        letter of our combined alphabet for this task.

        Your model should use a Recurrent Neural Network to summarize the list
        `xs` into a single node of shape (batch_size x hidden_size), for your
        choice of hidden_size. It should then calculate a node of shape
        (batch_size x 5) containing scores, where higher scores correspond to
        greater probability of the word originating from a particular language.

        Inputs:
            xs: a list with L elements (one per character), where each element
                is a node with shape (batch_size x self.num_chars)
        Returns:
            A node with shape (batch_size x 5) containing predicted scores
                (also called logits)
        """
        "*** YOUR CODE HERE ***"
        h = nn.ReLU(nn.AddBias(nn.Linear(xs[0], self.in_w), self.ini_b))
        for char in xs[1:]:
            linear1 = nn.Linear(char, self.xw)
            linear2 = nn.Linear(h, self.hw)
            add = nn.Add(linear1, linear2)
            add_bias = nn.AddBias(add, self.b)
            h = nn.ReLU(add_bias)
        return nn.AddBias(nn.Linear(h, self.out_w), self.outp_b)

    def get_loss(self, xs, y):
        """
        Computes the loss for a batch of examples.

        The correct labels `y` are represented as a node with shape
        (batch_size x 5). Each row is a one-hot vector encoding the correct
        language.

        Inputs:
            xs: a list with L elements (one per character), where each element
                is a node with shape (batch_size x self.num_chars)
            y: a node with shape (batch_size x 5)
        Returns: a loss node
        """
        "*** YOUR CODE HERE ***"
        y_h = self.run(xs)
        loss = nn.SoftmaxLoss(y_h, y)
        return loss

    def train(self, dataset):
        """
        Trains the model.
        """
        "*** YOUR CODE HERE ***"
        b_s = 100
        l = float('inf')
        v = 0
        while v < .84:
            for x, y in dataset.iterate_once(b_s):
                l = self.get_loss(x, y)
                grs = nn.gradients(l, self.params)
                l = nn.as_scalar(l)
                for param, grad in zip(self.params, grs):
                    param.update(grad, -self.t_a)
            v = dataset.get_validation_accuracy()
