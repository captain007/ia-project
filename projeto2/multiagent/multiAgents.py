# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent
from pacman import GameState

class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """


    def getAction(self, gameState: GameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {NORTH, SOUTH, WEST, EAST, STOP}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState: GameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        score = successorGameState.getScore()
        
        foodDistances = [manhattanDistance(newPos, food) for food in newFood.asList()]
        
        if foodDistances:
            minFoodDistance = min(foodDistances)
            score += 1.0 / (minFoodDistance + 1)

        for i in range(len(newGhostStates)):
            ghostState = newGhostStates[i]
            scaredTime = newScaredTimes[i]
            ghostPos = ghostState.getPosition()
            ghostDistance = manhattanDistance(newPos, ghostPos)
            ghostPos = ghostState.getPosition()
            ghostDistance = manhattanDistance(newPos, ghostPos)
        
            if scaredTime > 0:
                score += 10 / (ghostDistance + 1)
            else:
                if ghostDistance < 2:
                    score -= 10 / (ghostDistance + 1)

        return score
        return successorGameState.getScore()

def scoreEvaluationFunction(currentGameState: GameState):
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
    Your minimax agent (question 2)
    """

    def getAction(self, gameState: GameState):
        """
        Returns the minimax action from the current gameState using self.depth
        and self.evaluationFunction.

        Here are some method calls that might be useful when implementing minimax.

        gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

        gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

        gameState.getNumAgents():
        Returns the total number of agents in the game

        gameState.isWin():
        Returns whether or not the game state is a winning state

        gameState.isLose():
        Returns whether or not the game state is a losing state
        """
        "*** YOUR CODE HERE ***"
        legalActions = gameState.getLegalActions(0)
        bestAction = None
        bestScore = float("-inf")

        for action in legalActions:
            successorState = gameState.generateSuccessor(0, action)
            score = self.minValue(successorState, 1, 0)
            if score > bestScore:
                bestScore = score
                bestAction = action

        return bestAction

    def maxValue(self, gameState, depth):
        if gameState.isWin() or gameState.isLose() or depth == self.depth:
            return self.evaluationFunction(gameState)

        legalActions = gameState.getLegalActions(0)
        maxScore = float("-inf")

        for action in legalActions:
            successorState = gameState.generateSuccessor(0, action)
            score = self.minValue(successorState, 1, depth)
            maxScore = max(maxScore, score)

        return maxScore

    def minValue(self, gameState, agentIndex, depth):
        if gameState.isWin() or gameState.isLose():
            return self.evaluationFunction(gameState)

        legalActions = gameState.getLegalActions(agentIndex)
        minScore = float("inf")

        for action in legalActions:
            successorState = gameState.generateSuccessor(agentIndex, action)
            if agentIndex == gameState.getNumAgents() - 1:
                score = self.maxValue(successorState, depth + 1)
            else:
                score = self.minValue(successorState, agentIndex + 1, depth)
            minScore = min(minScore, score)

        return minScore
        util.raiseNotDefined()

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState: GameState):
        """
        Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        def minValue(state, alpha, beta, depth, agentIndex):
            if state.isWin() or state.isLose() or depth == self.depth:
                return self.evaluationFunction(state)

            v = float("inf")
            actions = state.getLegalActions(agentIndex)

            for action in actions:
                successor = state.generateSuccessor(agentIndex, action)
                v = min(v, maxValue(successor, alpha, beta, depth + 1))

                if v <= alpha:
                    return v
                beta = min(beta, v)

            return v

        def maxValue(state, alpha, beta, depth):
            if state.isWin() or state.isLose() or depth == self.depth:
                return self.evaluationFunction(state)

            v = float("-inf")
            actions = state.getLegalActions(0)

            for action in actions:
                successor = state.generateSuccessor(0, action)
                v = max(v, minValue(successor, alpha, beta, depth, 1))

                if v >= beta:
                    return v
                alpha = max(alpha, v)

            return v

        legalActions = gameState.getLegalActions(0)
        bestAction = None
        alpha = float("-inf")
        beta = float("inf")

        for action in legalActions:
            successorState = gameState.generateSuccessor(0, action)
            score = minValue(successorState, alpha, beta, 0, 1)

            if score > alpha:
                alpha = score
                bestAction = action

        return bestAction
        util.raiseNotDefined()

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState: GameState):
        """
        Returns the expectimax action using self.depth and self.evaluationFunction

        All ghosts should be modeled as choosing uniformly at random from their
        legal moves.
        """
        "*** YOUR CODE HERE ***"
        def maxValue(gameState, depth):
            if gameState.isWin() or gameState.isLose() or depth == self.depth:
                return self.evaluationFunction(gameState)

            legalActions = gameState.getLegalActions(0)
            maxScore = float("-inf")

            for action in legalActions:
                successorState = gameState.generateSuccessor(0, action)
                score = expValue(successorState, 1, depth)
                maxScore = max(maxScore, score)

            return maxScore

        def expValue(gameState, agentIndex, depth):
            if gameState.isWin() or gameState.isLose():
                return self.evaluationFunction(gameState)

            legalActions = gameState.getLegalActions(agentIndex)
            expScore = 0.0

            for action in legalActions:
                successorState = gameState.generateSuccessor(agentIndex, action)
                if agentIndex == gameState.getNumAgents() - 1:
                    score = maxValue(successorState, depth + 1)
                else:
                    score = expValue(successorState, agentIndex + 1, depth)
                expScore += score

            return expScore / len(legalActions)

        legalActions = gameState.getLegalActions(0)
        bestAction = None
        bestScore = float("-inf")

        for action in legalActions:
            successorState = gameState.generateSuccessor(0, action)
            score = expValue(successorState, 1, 0)
            if score > bestScore:
                bestScore = score
                bestAction = action

        return bestAction
        util.raiseNotDefined()

def betterEvaluationFunction(currentGameState: GameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
  
    newPos = currentGameState.getPacmanPosition()
    foodList = currentGameState.getFood().asList()
    ghostStates = currentGameState.getGhostStates()
    ghostPositions = [ghost.getPosition() for ghost in ghostStates]
    
    minFoodDistance = float('inf')
    for food in foodList:
        distance = manhattanDistance(newPos, food)
        if distance < minFoodDistance:
            minFoodDistance = distance
    if minFoodDistance == float('inf'):
        minFoodDistance = 0
    
    minGhostDistance = float('inf')
    for ghost in ghostPositions:
        distance = manhattanDistance(newPos, ghost)
        if distance < minGhostDistance:
            minGhostDistance = distance
    if minGhostDistance == float('inf'):
        minGhostDistance = 0
    
    capsules = currentGameState.getCapsules()
    numCapsules = len(capsules)
    
    if minGhostDistance > 0:
        ghostScore = 1.0 / minGhostDistance
    else:
        ghostScore = 0

    if minFoodDistance > 0:
        foodScore = 1.0 / minFoodDistance
    else:
        foodScore = 0
    
    score = currentGameState.getScore() + foodScore - ghostScore - numCapsules
    
    return score
    util.raiseNotDefined()

# Abbreviation
better = betterEvaluationFunction
